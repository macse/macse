Most multiple sequence alignment programs treat nucleotide sequences containing open reading frames without accounting for their underlying codon structure.

We present an algorithm especially designed for the multiple alignment of protein-coding sequences while accounting for the occurrence of premature stop-codons and frameshifts.

Our implementation generalizes the classical Smith-Waterman algorithm for aligning two nucleotide sequences while accommodating sequencing errors and other biological cases of deviations from the coding frame. The resulting pairwise coding-sequence alignment method was extended to a multiple sequence alignment (MSA) algorithm implemented in a program called MACSE (Multiple Alignment of Coding SEquences).

Tests on protein-coding benchmark datasets showed that MACSE outperforms classical MSA programs that ignore the coding nature of nucleotide sequences. On perfectly coding datasets, MACSE produces alignments close in accuracy to those obtained by indirect procedures aligning the amino-acid translation before reporting the inferred gap positions at the codon level. However, in cases where coding sequences contain frameshifts and/or stop-codons, our method achieves high accuracy on datasets that cannot be aligned by any other method without disrupting the codon structure.

MACSE proved particularly useful in detecting unnoticed frameshifts in public database sequences, for aligning protein-coding genes containing non-functional sequences (pseudogenes) without disrupting the underlying codon structure, and for mapping next-generation sequencing reads against a reference coding sequence.

When using MACSE, please cite :
MACSE : Multiple Alignment of Coding SEquences accounting for frameshifts and stop codons. Vincent Ranwez, Sébastien Harispe, Frédéric Delsuc, Emmanuel JP Douzery Plos One 6(9) : e22594.
PDF
WEB Server#

NEW MACSE version (0.9b1)
This version is much less memory demanding than the previous one (0.8b2).
Source code of MACSE can be downloaded, using this repository.